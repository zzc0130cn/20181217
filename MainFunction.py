import xlrd
import pygal

# 导入excel，创建表格对象
Workbook_obj = xlrd.open_workbook("./导出数据_一般企业.xlsx")
main_sheet = Workbook_obj.sheet_by_index(0)


def draw_hist(x_labels, x_title, y_title, data_title, data, title, output):
    # 画出柱状图 x轴坐标，x轴名称，y轴坐标，y轴名称，数据名称，数据，输出路径及文件名
    hist = pygal.Bar()
    hist.title = title
    hist.x_labels = x_labels
    hist.x_title = x_title
    hist.y_title = y_title
    hist.add(data_title, data)
    hist.render_to_file(output)


def create_temp_list(tlist):
    # 分离公司名和数据 二维列表 返回二维列表（公司名，数据）
    new_name_list = []
    new_number_list = []

    for i in range(len(tlist)):
        new_name_list.append(tlist[i][0])
        new_number_list.append(tlist[i][1])

    return [new_name_list, new_number_list]

# 1、通过excel，使用E列公允价值变动净收益/F列归属母公司股东的净利润

# 获取列数据


b_column = main_sheet.col_values(1, 1)
e_column = main_sheet.col_values(4, 1)
f_column = main_sheet.col_values(5, 1)

# 去除空值，生成新二维列表
new_sheet = []
for i in range(len(e_column)):
    if e_column[i] == 0 or e_column[i] == '':
        continue
    else:
        new_sheet.append([b_column[i], e_column[i] / f_column[i]])

# 排序
result1 = sorted(new_sheet, key=lambda x: x[1], reverse=True)

# 创建临时列表用户绘图
temp_list = create_temp_list(result1)

# 绘图
draw_hist(temp_list[0][0:10], '公司名', '收益/净利润', '收益/净利润', temp_list[1][0:10],
          '公允价值变动净收益/归属母公司股东的净利润前十位', './result-1.svg')
draw_hist(temp_list[0][-10:], '公司名', '收益/净利润', '收益/净利润', temp_list[1][-10:],
          '公允价值变动净收益/归属母公司股东的净利润后十位', './result-2.svg')

# 通过excel表格，将企业年化收益率从高到低排列


# 获取列数据
j_column = main_sheet.col_values(9, 1)

# 去除空值，生成新二维列表
new_sheet = []
for i in range(len(j_column)):
    if j_column[i] == 0 or j_column[i] == '':
        continue
    else:
        new_sheet.append([b_column[i], j_column[i]])
result2 = sorted(new_sheet, key=lambda x: x[1], reverse=True)

# 创建临时列表用户绘图
temp_list = create_temp_list(result2)

# 绘图
draw_hist(temp_list[0][0:10], '公司名', '%', '年化收益率', temp_list[1][0:10],
          '企业年化收益率前十位（60个月）', './result-3.svg')

# 通过excel表格，将企业收益年化波动性率按从高到低排列


# 获取列数据
h_column = main_sheet.col_values(7, 1)

# 去除空值，生成新二维列表
new_sheet = []
for i in range(len(h_column)):
    if h_column[i] == 0 or h_column[i] == '':
        continue
    else:
        new_sheet.append([b_column[i], h_column[i]])
result3 = sorted(new_sheet, key=lambda x: x[1], reverse=True)

# 创建临时列表用户绘图
temp_list = create_temp_list(result3)

# 绘图
draw_hist(temp_list[0][0:10], '公司名', '%', '收益年化波动性率', temp_list[1][0:10],
          '企业收益年化波动性率前十位（60个月）', './result-4.svg')

# 通过excel表格，让BETA值(最近100周)按从大到小排列


# 获取列数据
k_column = main_sheet.col_values(10, 1)

# 去除空值，生成新二维列表
new_sheet = []
for i in range(len(k_column)):
    if k_column[i] == 0 or k_column[i] == '':
        continue
    else:
        new_sheet.append([b_column[i], k_column[i]])
result4 = sorted(new_sheet, key=lambda x: x[1], reverse=True)

# 创建临时列表用户绘图
temp_list = create_temp_list(result4)

# 绘图
draw_hist(temp_list[0][0:10], '公司名', 'BETA', 'BETA值', temp_list[1][0:10], 'BETA值(最近100周)',
          './result-5.svg')

# 通过excel表格，让BETA值(最近60个月)按从大到小排列


# 获取列数据
l_column = main_sheet.col_values(11, 1)

# 去除空值，生成新二维列表
new_sheet = []
for i in range(len(l_column)):
    if l_column[i] == 0 or l_column[i] == '':
        continue
    else:
        new_sheet.append([b_column[i], l_column[i]])
result5 = sorted(new_sheet, key=lambda x: x[1], reverse=True)

# 创建临时列表用户绘图
temp_list = create_temp_list(result5)

# 绘图
draw_hist(temp_list[0][0:10], '公司名', 'BETA', 'BETA值', temp_list[1][0:10], 'BETA值(最近60个月)',
          './result-6.svg')


